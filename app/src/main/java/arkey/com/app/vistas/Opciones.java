package arkey.com.app.vistas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import arkey.com.app.R;
import arkey.com.app.contenedor.Container;
import arkey.com.app.persistencia.DB;

public class Opciones extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    private Button guardar, mostrar;
    private Switch musica;
    private DB db;
    private boolean isOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones);
        //Obteniendo instancia de db
        db = Container.getDB(this.getApplicationContext());
        //Enlazando widgets
        bindWidgets();
        //set listeners
        setListeners();
        //Configurando el switch
        setSwitch();

    }

    private void bindWidgets(){
        guardar = (Button) findViewById(R.id.save);
        mostrar = (Button) findViewById(R.id.puntaje);
        musica = (Switch) findViewById(R.id.musica);
    }

    private void setListeners(){
        guardar.setOnClickListener(this);
        mostrar.setOnClickListener(this);
        musica.setOnCheckedChangeListener(this);
    }

    private void setSwitch(){
        musica.setChecked(db.getMusica());
    }

    @Override
    public void onClick(View v) {
        //Eventualmente habran mas botones
        switch (v.getId()){
            case R.id.save:
                db.setMusica(isOn);
                Toast.makeText(this, getResources().getString(R.string.preferencias), Toast.LENGTH_SHORT).show();
                break;

            case R.id.puntaje:
                Intent i = new Intent(this.getApplicationContext(), Puntaje.class);
                startActivity(i);
                break;
        }


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        isOn = isChecked;
    }
}
