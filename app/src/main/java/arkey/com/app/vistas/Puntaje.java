package arkey.com.app.vistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import arkey.com.app.R;
import arkey.com.app.contenedor.Container;
import arkey.com.app.persistencia.DB;

public class Puntaje extends AppCompatActivity {

    private TextView puntaje;
    private HashMap<String, Double> puntajes;
    private StringBuilder cadena;
    private DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puntaje);
        db = Container.getDB(this.getApplicationContext());
        puntaje = (TextView) findViewById(R.id.puntaje);
        listaPuntajes();
        puntaje.setText(cadena.toString());
    }

    //Extrayendo puntajes
    private void listaPuntajes(){
        cadena = new StringBuilder();
        cadena.append("----------------\n");
        puntajes = db.mostrarPuntajes();
        for (Map.Entry<String, Double> entry : puntajes.entrySet()){
                cadena.append(entry.getKey() + ": " + entry.getValue() + "\n");
            cadena.append("-----------------\n");
        }
    }
}
