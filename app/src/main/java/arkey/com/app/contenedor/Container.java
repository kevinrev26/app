package arkey.com.app.contenedor;

import android.content.Context;

import arkey.com.app.persistencia.DB;

/**
 * Created by kevin on 12-18-16.
 */

public class Container {

    private static DB db;



    public static DB getDB(Context ctx){
        if(db==null){
            db = new DB(ctx);
        }

        return db;
    }

}
