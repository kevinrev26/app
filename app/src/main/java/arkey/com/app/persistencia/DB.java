package arkey.com.app.persistencia;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by kevin on 12-17-16.
 */

public class DB {

    private Context ctx;
    private SharedPreferences preferencias;
    private final String ARCHIVO = "app";
    public final String FACIL = "facil";
    public final String MEDIO = "medio";
    public final String DIFICIL = "dificil";
    private final String MUSICA = "musica";

    public DB(Context ctx) {
        this.ctx = ctx;
    }

    private void instanciar(){
        if (preferencias==null){
            preferencias = ctx.getSharedPreferences(ARCHIVO,ctx.MODE_PRIVATE);
        }
    }

    public void guardarPuntaje(String dificultad, double puntaje){
        //Verificando que existe una y solo una instancia de SharedPreferences
        instanciar();
        //Recuperando el puntaje almacenado en las preferencias en la dificultad especificada
        Double actual = Double.longBitsToDouble(preferencias.getLong(dificultad, 0));
        //Si el actual es menor que el puntaje, entonces almacenar
        //Caso contrario no hacer nada
        if(actual<puntaje){
            SharedPreferences.Editor editor = preferencias.edit();
            editor.putLong(dificultad, Double.doubleToLongBits(puntaje));
            editor.commit();
        }

    }

    public HashMap<String, Double> mostrarPuntajes(){
        //Creando estructura map para almacenar los puntajes
        HashMap<String, Double> puntajes = new HashMap<String, Double>();
        instanciar();
        //Recuperando los puntajes
        Double facil = Double.longBitsToDouble(preferencias.getLong(FACIL,0));
        puntajes.put(FACIL,facil);
        Double medio = Double.longBitsToDouble(preferencias.getLong(MEDIO,0));
        puntajes.put(MEDIO,medio);
        Double dificil = Double.longBitsToDouble(preferencias.getLong(DIFICIL,0));
        puntajes.put(DIFICIL,dificil);
        return puntajes;
    }

    public void setMusica(boolean musica){
        instanciar();
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putBoolean(MUSICA,musica);
        editor.commit();
    }

    public boolean getMusica(){
        instanciar();
        return preferencias.getBoolean(MUSICA,true);
    }


}
