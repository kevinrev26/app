package arkey.com.app;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import arkey.com.app.contenedor.Container;
import arkey.com.app.persistencia.DB;
import arkey.com.app.vistas.Opciones;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button opciones, salir, comenzar, generar;
    private DB db;
    private MediaPlayer mp;
    private TextView dificultadText;
    private String[] dificultades = {"facil", "medio", "dificil"};
    private int indice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Obteniendo los widgets de la vista para manipularlos desde el codigo
        bindWidgets();
        //Estableciendo los listeners
        setListeners();
        db = Container.getDB(this.getApplicationContext());
        dificultadText.setText(dificultades[indice]);
        reproducir();
    }


    private void bindWidgets(){
        opciones = (Button) findViewById(R.id.opcion);
        salir = (Button) findViewById(R.id.exit);
        comenzar = (Button) findViewById(R.id.init);
        generar = (Button) findViewById(R.id.generar);
        dificultadText = (TextView) findViewById(R.id.dificultadText);
    }

    private void setListeners(){
        opciones.setOnClickListener(this);
        salir.setOnClickListener(this);
        comenzar.setOnClickListener(this);
        generar.setOnClickListener(this);
        dificultadText.setOnClickListener(this);
    }

    private void generarPuntaje(){
        Random r = new Random();
        double facil = 0 + r.nextDouble();
        db.guardarPuntaje("facil", facil);
        double medio = 0 +r.nextDouble();
        db.guardarPuntaje("medio",medio);
        double dificil = 0 + r.nextDouble();
        db.guardarPuntaje("dificil", dificil);
    }

    private void reproducir(){
        if (db.getMusica()){
            mp = new MediaPlayer();
            mp = MediaPlayer.create(this.getApplicationContext(), R.raw.song1);
            mp.start();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.exit:
                Toast.makeText(this, getResources().getString(R.string.finalizar), Toast.LENGTH_SHORT).show();
                this.finish();
                break;
            case R.id.init:
                Toast.makeText(this, getResources().getString(R.string.iniciar), Toast.LENGTH_SHORT).show();
                break;
            case R.id.generar:
                //llamada a la funcion
                generarPuntaje();
                Toast.makeText(this, "Click!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opcion:
                Intent i =  new Intent(this.getApplicationContext(), Opciones.class);
                startActivity(i);
                break;
            case R.id.dificultadText:
                setDificultad();
                break;
        }
    }


    private void setDificultad(){
        indice++;
        if (indice >= dificultades.length){
            indice = 0;
        }
        dificultadText.setText(dificultades[indice]);
    }


/*    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }*/
}
